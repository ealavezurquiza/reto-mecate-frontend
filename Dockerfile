FROM node:16

RUN apt-get update && apt-get install -y bash

WORKDIR /angular

COPY package*.json .

RUN npm i

EXPOSE 4200

COPY . .

CMD [ "npm", 'start' ]
